import React from "react";
import "../../App.css";
import Footer from "../Footer";
import Cards from "../Cards";

// export default function events() {
//   return <h1 className="events">Events</h1>;
// }

export default function events() {
  return (
    <>
      {/* <h1 className="events"></h1>; */}
      <Cards />
      <Footer />
    </>
  );
}
