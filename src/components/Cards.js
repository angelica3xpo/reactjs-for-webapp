import React from "react";
import "./Cards.css";
import CardItem from "./CardItem";
import { useState } from "react";
import Axios from "axios";

function Cards() {
  const [name, setName] = useState("");
  const [reps, setReps] = useState("0");

  const [newReps, setNewReps] = useState("0");

  const [exhibitorList, setExhibitorList] = useState([]);

  // insert details on the db
  const addExhibitor = () => {
    Axios.post("http://localhost:3001/create", {
      name: name,
      reps: reps,
    }).then(() => {
      setExhibitorList([
        //array destructor to add new values to the table without refreshing the page
        ...exhibitorList,
        {
          name: name,
          reps: reps,
        },
      ]);
    });
  };

  // get details on the db
  const getExhibitors = () => {
    Axios.get("http://localhost:3001/exhibitors").then((response) => {
      setExhibitorList(response.data);
    });
  };

  // update details
  const updateExhibitorReps = (id) => {
    Axios.put("http://localhost:3001/update", { reps: newReps, id: id }).then(
      (response) => {
        //update data without refreshing - not working == bug
        setExhibitorList(
          exhibitorList.map((val) => {
            return val.id == id
              ? { id: val.id, name: val.name, reps: val.newReps }
              : val;
          })
        );
      }
    );
  };

  // delete
  const deleteExhibitor = (id) => {
    Axios.delete(`http://localhost:3001/delete/${id}`).then((response) => {
      setExhibitorList(
        exhibitorList.filter((val) => {
          return val.id != id;
        })
      );
    });
  };

  return (
    <div className="cards">
      <h1>Check out these future virtual events!</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="images/img-9.jpg"
              text="3XPO DEMO EVENT 1"
              label="11/05/2021"
              path="/events"
            />
            <CardItem
              src="images/img-2.jpg"
              text="3XPO DEMO EVENT 2"
              label="12/01/2021"
              path="/events"
            />
          </ul>
          <ul className="cards__items">
            <CardItem
              src="images/img-3.jpg"
              text="3XPO DEMO EVENT 3"
              label="SOON"
              path="/events"
            />
            <CardItem
              src="images/img-4.jpg"
              text="3XPO DEMO EVENT 4"
              label="SOON"
              path="/more"
            />
            <CardItem
              src="images/img-8.jpg"
              text="3XPO DEMO EVENT 5"
              label="SOON"
              path="/sign-up"
            />
            <CardItem
              src="images/img-4.jpg"
              text="3XPO DEMO EVENT 6"
              label="SOON"
              path="/more"
            />
          </ul>
        </div>
      </div>

      <div className="App">
        <label> Exhibitor Name: </label>
        <input
          type="text"
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <label> Number of Reps: </label>
        <input
          type="number"
          onChange={(event) => {
            setReps(event.target.value);
          }}
        />
        <button onClick={addExhibitor}>Add Exhibitor</button>
      </div>

      <br />
      <div className="exhibitors">
        <p>View Exhibitors</p>
        <button onClick={getExhibitors}> Show Exhibitors</button>

        {exhibitorList.map((val, key) => {
          return (
            <div className="viewexhibitors">
              <div>
                <h3>Name: {val.name}</h3>
                <h3>Number of Exhibitors: {val.reps}</h3>
              </div>
              {/* update data */}
              <div>
                <input
                  type="text"
                  placeholder="2000..."
                  onChange={(event) => {
                    setNewReps(event.target.value);
                  }}
                />
                <button
                  onClick={() => {
                    updateExhibitorReps(val.id);
                  }}
                >
                  Update
                </button>
                <button
                  onClick={() => {
                    deleteExhibitor(val.id);
                  }}
                >
                  Delete
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Cards;
