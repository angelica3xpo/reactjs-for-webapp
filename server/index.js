const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  user: "root",
  host: "localhost",
  password: "",
  database: "3xposystem",
});

// standard for creating request using express js

app.post("/create", (req, res) => {
  console.log(req.body);
  const name = req.body.name;
  const reps = req.body.reps;

  db.query(
    "INSERT INTO exhibitors (name, reps) VALUES (?,?)",
    [name, reps],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
});

// standard for creating request using express js
app.get("/exhibitors", (req, res) => {
  db.query("SELECT * FROM exhibitors", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

// update reps - number
app.put("/update", (req, res) => {
  const id = req.body.id;
  const reps = req.body.reps;
  db.query(
    "UPDATE exhibitors SET reps = ? WHERE id = ?",
    [reps, id],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// delete
app.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query("DELETE FROM exhibitors WHERE id = ?", id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.listen(3001, () => {
  console.log("server is running on port 3301 ");
});
